"use strict";

const Sequelize = require('sequelize'),
    {
        fullUrl
    } = require('./request');

/**
 *
 * @type {string}
 */
const ErrorLayoutParameterSentence = 'Parameter %s not found';

const mappingError = {
    0: {
        layout: ErrorLayoutParameterSentence,
        status: 404,
        code: 404
    }
};

/**
 *
 * @param ErrorType
 * @param ParameterName
 * @param stack
 * @returns {*}
 * @constructor
 */
const ErrorWrapper = (ErrorType, ParameterName, stack = {}) => {
    try {
        const errorData = mappingError[ErrorType];
        const error = new Error(errorData.layout.replace('%s', ParameterName));
        error.detail = stack;
        error.status = errorData.status;
        error.code = errorData.code;

        return error;

    } catch (error) {
        logger.error(error);
        return error;
    }
};

/**
 *
 * @param error
 * @param req
 * @param mergeData
 * @param type - array 0 - console.log, 1 - save to db
 * @returns {{status: number, error: boolean, message: *, timestamp: number, path: *}}
 */

const formattingError = (error, req, mergeData = {}, type = {}) => {

    let errorWrapper = null;
    let errorDetail = null;

    if (typeof error === "object" && error.hasOwnProperty('name')) {
        const errorUniqueConstraintError = `Sequelize${Sequelize.UniqueConstraintError.name}`;
        const errorValidationErrorItem = `Sequelize${Sequelize.ValidationErrorItem.name}`;
        const errorValidationError = `Sequelize${Sequelize.ValidationError.name}`;
        const errorAssociationError = `SequelizeAssociationError`;
        const errorConnectionError = `Sequelize${Sequelize.ConnectionError.name}`;
        const errorDatabaseError = `Sequelize${Sequelize.DatabaseError.name}`;
        const SequelizeEagerLoadingError = `SequelizeEagerLoadingError`;
        const assertionError = 'AssertionError [ERR_ASSERTION]';
        const AuthenticationError = 'AuthenticationError';

        switch (error.name) {
            case  errorUniqueConstraintError :
            case  errorValidationErrorItem :
            case  errorValidationError :
            case  errorConnectionError : {
                errorWrapper = error.errors.map((error) => error.message)[0];
                errorDetail = error;
            };break;
            case  errorDatabaseError : {

                errorWrapper = `${error.original.name} code ${error.original.code}`;
                errorDetail = error;
            };break;

            case  assertionError: {
                errorWrapper = "The requested entity was not found in the database";
                errorDetail = error;
            };break;

            case  errorAssociationError:
            case  SequelizeEagerLoadingError:
            case  AuthenticationError: {

                errorWrapper = error.message;
                errorDetail = error;
            };break;

            default : {
                errorWrapper = error;
                errorDetail = error;
            };break;

        }
    } else {
        errorWrapper = errorDetail = error !== void 0 ? error.message : error;
    }

    const errorStructure = {
        "status": error !== void 0 && error.status ? error.status : 1,
        "error": true,
        "message": errorWrapper,
        "timestamp": new Date() * 1,
        "code": error !== void 0 && error.code ? error.code : null,
        "detail": error.stack,
        "path": req ? fullUrl(req) : null,
        ...mergeData
    };

    return errorStructure
};


module.exports = {
    formattingError,
    ErrorWrapper
};