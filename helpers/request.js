/**
 *
 * @param req
 * @returns {{protocol: (string|*|string|"_debugger".Protocol|String), host, pathname: *}}
 */
const fullUrl = (req) => {
    return {
        protocol: req.protocol,
        host: req.get('host'),
        pathname: req.originalUrl
    };
};


module.exports = {
    fullUrl
};


