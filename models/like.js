'use strict';
module.exports = (sequelize, DataTypes) => {
    var Likes = sequelize.define('Likes', {
        username: {
            type: DataTypes.STRING,
            allowNull: false
        },
        post_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        timestamp: {
            type: DataTypes.DATE,
            defaultValue: new Date()*1
        }
    }, {
        timestamps: true,
        createdAt: false,
        updatedAt: false
    });

    Likes.associate = function (models) {
        // associations can be defined here
    };
    return Likes;
};