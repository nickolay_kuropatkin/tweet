'use strict';
module.exports = (sequelize, DataTypes) => {
    var Tweets = sequelize.define('Tweets', {
        content: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        username: {
            type: DataTypes.STRING,
            allowNull: false
        },
        timestamp: {
            type: DataTypes.DATE,
            defaultValue: new Date()*1
        }
    }, {
        timestamps: true,
        createdAt: false,
        updatedAt: false
    });

    Tweets.associate = function (models) {
        // associations can be defined here
    };

    return Tweets;
};