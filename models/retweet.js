'use strict';
module.exports = (sequelize, DataTypes) => {
    var Retweets = sequelize.define('Retweets', {
        post_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        username: {
            type: DataTypes.STRING,
            allowNull: false
        },
        timestamp: {
            type: DataTypes.DATE,
            defaultValue: new Date()*1
        }
    }, {
        timestamps: true,
        createdAt: false,
        updatedAt: false
    });
    Retweets.associate = function (models) {
        // associations can be defined here
    };
    return Retweets;
};