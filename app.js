var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
const httpCodeStatus = require('http-status-codes');
const { formattingError } = require('./helpers/error');
const tweetsRouter = require('./routes/tweets');
const retweetsRouter = require('./routes/retweets');

var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
// security server block
app.use(helmet());
app.use(helmet.noCache());
app.disable('x-powered-by');
//end security server block
// cors start
app.use(cors());
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/tweets', tweetsRouter);
app.use('/retweets', retweetsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = httpCodeStatus.NOT_FOUND;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // render the error page
    res.status(err.status || httpCodeStatus.INTERNAL_SERVER_ERROR);
    res.json(formattingError(err, req));
    next(err);
});

module.exports = app;
