'use strict';

const express = require('express'),
    router = express.Router(),
    Sequilize = require('sequelize'),
    models = require('../models'),
    isInteger = require('lodash.isinteger'),
    {formattingError} = require('./../helpers/error'),
    statusHttpCode = require('http-status-codes');

router.get('/', async (req, res) => {
    try {

        models.Tweets.hasMany(models.Likes, {
            foreignKey: {
                name: 'post_id',
                allowNull: false
            }
        });

        models.Tweets.hasMany(models.Retweets, {
            foreignKey: {
                name: 'post_id',
                allowNull: false
            }
        });

        const tweets = await models.Tweets.findAll({
            raw: true,
            include: [
                {
                    model: models.Likes,
                    attributes: [Sequilize.literal('count("Likes"."post_id") as likes_count')],
                },
                {
                    model: models.Retweets,
                    attributes: [Sequilize.literal('count("Retweets"."post_id") as retweets_count')],
                }
            ],
            group: ['Tweets.id', 'Likes.post_id', 'Retweets.post_id']
        });

        res.status(statusHttpCode.OK).json({"status": statusHttpCode.OK, "data": {tweets}})
    } catch (error) {
        res.status(error.status || statusHttpCode.INTERNAL_SERVER_ERROR).json(formattingError(error, req));
    }
});

router.post('/', async (req, res) => {
    try {
        if (req.body.content === void 0) {
            const error = new Error('Parameter content not found');
            error.status = statusHttpCode.NOT_FOUND;

            throw error;
        }

        if (req.body.username === void 0) {
            const error = new Error('Parameter content not found');
            error.status = statusHttpCode.NOT_FOUND;

            throw error;
        }

        models.Tweets.create(req.body)
            .then((tweet) => {

                res.status(statusHttpCode.CREATED).json({"status": statusHttpCode.CREATED, tweet})
            }).catch((error) => {
            res.status(error.status || statusHttpCode.CONFLICT).json(formattingError(error, req));
        });

    } catch (error) {
        res.status(error.status || statusHttpCode.INTERNAL_SERVER_ERROR).json(formattingError(error, req));
    }
});

router.post('/:id/likes', async (req, res) => {
    try {
        if (req.body.username === void 0) {
            const error = new Error('Parameter username not found');
            error.status = statusHttpCode.NOT_FOUND;

            throw error;
        }

        const tweetId = parseInt(req.params.id);

        if (!isInteger(tweetId)) {
            const error = new Error('Parameter id isn`t int');
            error.status = statusHttpCode.INTERNAL_SERVER_ERROR;

            throw error;
        }

        const likesFormatedData = {post_id: tweetId, username: req.body.username};

        models.Likes.create(likesFormatedData)
            .then((like) => {

                res.status(statusHttpCode.CREATED).json({"status": statusHttpCode.CREATED, like})
            }).catch((error) => {
            res.status(error.status || statusHttpCode.CONFLICT).json(formattingError(error, req));
        });

    } catch (error) {
        res.status(error.status || statusHttpCode.INTERNAL_SERVER_ERROR).json(formattingError(error, req));
    }
});


router.post('/:id/retweet', async (req, res) => {
    try {
        if (req.body.username === void 0) {
            const error = new Error('Parameter username not found');
            error.status = statusHttpCode.NOT_FOUND;

            throw error;
        }

        const tweetId = parseInt(req.params.id);

        if (!isInteger(tweetId)) {
            const error = new Error('Parameter id isn`t int');
            error.status = statusHttpCode.INTERNAL_SERVER_ERROR;

            throw error;
        }

        const retweetsFormatedData = {post_id: tweetId, username: req.body.username};

        models.Retweets.create(retweetsFormatedData)
            .then((retweet) => {

                res.status(statusHttpCode.CREATED).json({"status": statusHttpCode.CREATED, retweet})
            }).catch((error) => {
            res.status(error.status || statusHttpCode.CONFLICT).json(formattingError(error, req));
        });

    } catch (error) {
        res.status(error.status || statusHttpCode.INTERNAL_SERVER_ERROR).json(formattingError(error, req));
    }
});

module.exports = router;