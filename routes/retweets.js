'use strict';

const express = require('express'),
    router = express.Router(),
    Sequilize = require('sequelize'),
    models = require('../models'),
    isInteger = require('lodash.isinteger'),
    {formattingError} = require('./../helpers/error'),
    statusHttpCode = require('http-status-codes');

router.get('/', async (req, res) => {
    try {
         models.Retweets.belongsTo(models.Tweets, {foreignKey: 'post_id', targetKey: 'id'});

        const retweets = await models.Retweets.findAll({
            raw: true,
            attributes: [
                Sequilize.literal('"Retweets"."username" as retweet_user'),
                Sequilize.literal('to_char("Retweets"."timestamp" at time zone \'UTC\', \'YYYY-MM-DD"T"HH24:MI:SSZ\') as timestamp')
            ],
            include: [
                {
                    model: models.Tweets,
                    attributes: [
                        Sequilize.literal('"Tweet"."id" as tweet_id'),
                        Sequilize.literal('"Tweet"."username" as tweet_user'),
                        Sequilize.literal('"Tweet"."content" as content'),
                    ],
                }]
        });

        res.status(statusHttpCode.OK).json({"status": statusHttpCode.OK, "data": {retweets}})
    } catch (error) {
        res.status(error.status || statusHttpCode.INTERNAL_SERVER_ERROR).json(formattingError(error, req));
    }
});

module.exports = router;