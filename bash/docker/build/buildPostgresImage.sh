#!/usr/bin/env bash
source `pwd`/docker/.env
source `pwd`/bash/docker/build/constants.sh

BASE_PATH_POSTGRES=${BASE_PATH}/docker/postgres
FILE_PATH=${BASE_PATH_POSTGRES}/postgres_version.txt

get_version() {
    echo `awk -v PARAMETER="$1" -v COUNT="$2" -f ${AWK_PARSER_PATH} ${FILE_PATH}`
}

MAJOR=`get_version 1 0`
MINOR=`get_version 2 0`
VERSION=`get_version 3 1`

docker build --no-cache=true -t ${DOCKER_POSTGRES_TAG_NAME}:$MAJOR.$MINOR.$VERSION -t ${DOCKER_POSTGRES_TAG_NAME}:latest $BASE_PATH_POSTGRES

docker tag ${DOCKER_POSTGRES_TAG_NAME}:$MAJOR.$MINOR.$VERSION ${REGISTRY_HOST}/${DOCKER_POSTGRES_TAG_NAME}:$MAJOR.$MINOR.$VERSION
docker tag ${DOCKER_POSTGRES_TAG_NAME}:latest ${REGISTRY_HOST}/${DOCKER_POSTGRES_TAG_NAME}:latest

docker push ${REGISTRY_HOST}/${DOCKER_POSTGRES_TAG_NAME}:$MAJOR.$MINOR.$VERSION
docker push ${REGISTRY_HOST}/${DOCKER_POSTGRES_TAG_NAME}:latest

echo "$MAJOR.$MINOR.$VERSION" > $FILE_PATH
