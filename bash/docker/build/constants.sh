#!/usr/bin/env bash

BASE_PATH=`pwd`
REGISTRY_HOST=ns528503.ip-149-56-20.net:5000
AWK_PARSER_PATH=${BASE_PATH}/bash/docker/build/parse_version.awk
DOCKER_SERVER_TAG_NAME='dev/server'
DOCKER_POSTGRES_TAG_NAME='dev/postgres'