# SOURCE
    https://hub.docker.com/_/postgres/

## DOCKER RUN
    docker run -t -i -d -p 5432:5432 -v /var/www/docker_postgres_data:/var/lib/postgresql/data postgres
## Go to docker image database
    psql -h localhost -p 5432 -U postgres
## rm docker volume
    docker volume rm docker_db_data