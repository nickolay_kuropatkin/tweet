#!/bin/bash

shift

until psql -h "$DOCKER_DATABASE_HOST" -U "postgres" -c '\l'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 npm i && $SERVER_PATH